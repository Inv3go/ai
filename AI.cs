﻿using System.Collections;
using System.Numerics;
using ImGuiNET;
using PoeHUD.Hud.Settings;
using PoeHUD.Models;
using PoeHUD.Plugins;

namespace AI
{
    public class AI:BaseSettingsPlugin<AISettings>
    {
        public override void EntityAdded(EntityWrapper entityWrapper)
        {
        }

        public override void EntityRemoved(EntityWrapper entityWrapper)
        {
        }

        public override void Initialise()
        {
        }

        private string LastAction = "Test";
        public override void Render()
        {
            bool test = Settings.Enable.Value;
            ImGui.SetNextWindowPos(new System.Numerics.Vector2(200,200),Condition.Appearing,Vector2.One);
            ImGui.SetNextWindowSize(new Vector2(800,600),Condition.Appearing);
            ImGui.BeginWindow("AI",ref test,WindowFlags.Default);
            Settings.Enable.Value = test;
            ImGui.Text($"Action: {LastAction}");
            ImGui.EndWindow();
        }


        IEnumerator MainLogic()
        {
            yield return null;
        }

        IEnumerator ExploreMap()
        {
            yield return null;
        }

        IEnumerator AttackEnemy()
        {
            yield return null;
        }

        IEnumerator PickUpItems()
        {
            yield return null;
        }

        IEnumerator OpenStrongBox()
        {
            yield return null;
        }

        IEnumerator OpenSmallChest()
        {
            yield return null;
        }

        IEnumerator DestroySmallObject()
        {
            yield return null;
        }
        
        
    }

    public class AISettings : SettingsBase
    {
        public AISettings()
        {
            Enable = true;
        }
    }
}